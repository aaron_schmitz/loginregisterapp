<?php
// Before a user can log in we have to check whether the email is present and if that email exists, the account should be set to active
// If not -> Ask the user to check his/her mails
if(isset($_POST['mail']) && !empty($_POST['mail']) AND isset($_POST['password']) && !empty($_POST['password']))
{
	require('constants.php');
	
	try
	{
		$email_encrypt = urlencode($_POST['mail']);
		$pw_encrypt = urlencode($_POST['password']);
		$pw = hash("sha256", $pw_encrypt);
		$pw_to_check = "";
		$active = 0;
		
		$pdo = new PDO('mysql:host='.HOST.';dbname='.DBNAME.';charset=utf8', DBUSER, DBPW);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$stmt = $pdo->prepare("SELECT * FROM user WHERE email = :email");
		$stmt->bindParam(':email', $email_encrypt);
		$stmt->execute();
		
		if($stmt->rowCount() > 0)
		{
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$pw_to_check = $row["pw"];
				$active = $row["active"];
			}
			
			if($pw_to_check === $pw)
			{
				if($active === 1)
				{
					session_start();
					$_SESSION["logged_in"] = true;
					header("Location: ?pageid=safe");
				}
				else
				{
					echo "<p>Account has not been activated yet!</p>";
				}
			}
			else
			{
				echo "<p>Incorrect password</p>";
			}
		}
		else
		{
			echo "<p>Email has not been registered so far!</p>";
		}
	}
	catch(PDOException $ex)
	{
		echo $ex->getMessage();
	}
}
?>

<h1>Login</h1>
<!-- I'm using tables here but using fieldsets would be a better approach -->
<form action="" method="post">
	<table>
		<tr>
			<td><label for="mail">Email:</label></td>
			<td><input type="text" id="mail" name="mail"></td>
		</tr>
		<tr>
			<td><label for="password">Password:</label></td>
			<td><input type="password" id="password" name="password"></td>
		</tr>
		<tr>
			<td><input type="submit" id="button" name="button" value="send"></td>
		</tr>
	</table>
</form>