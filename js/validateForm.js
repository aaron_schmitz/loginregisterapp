<script>
	// JavaScript validation of user inputs
	function validate(t)
	{
		var firstname = t.firstname.value;
		var lastname = t.lastname.value;
		var mail = t.mail.value;
		var password = t.password.value;
		var passwordc = t.passwordc.value;
		
		if(firstname == null || lastname == null || mail == null || password == null || passwordc == null)
		{
			alert("Bitte alle Felder ausfüllen bzw. bitte wählen Sie einen Grund aus!");
			return false;
		}
		else
		{	
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var check = re.test(mail);
			if(check)
			{
                if(password !== passwordc)
				{
					alert("Password and password confirmation do no match!");
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				alert("The email adress you have entered does not match the pattern: a@b.com!");
				return false;
			}
		}
	}
</script>
