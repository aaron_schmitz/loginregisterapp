<h1>Activation Page</h1>
<?php
// Here I check whether the key is present in the DB and if so, activate the account belonging to the hash

// Init
require('constants.php');

// Hash present?
if(isset($_GET['key']) AND !empty($_GET['key']))
{
	// Check for hash in :link
	try
	{
		$pdo = new PDO('mysql:host='.HOST.';dbname='.DBNAME.';charset=utf8', DBUSER, DBPW);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$stmt = $pdo->prepare("SELECT * FROM user WHERE link = :link");
		$stmt->bindParam(':link', $_GET['key']);
		$stmt->execute();
	}
	catch(PDOException $ex)
	{
		echo $ex->getMessage();
	}

	if($stmt->rowCount() > 0)
	{
		// If there is 1 entry found, activate that account
		$activate = 1;
		$stmt = $pdo->prepare("UPDATE user SET active = :active WHERE link = :link");
		$stmt->bindParam(':active', $activate);
		$stmt->bindParam(':link', $_GET['key']);
		$stmt->execute();
		echo "<p>Your account is now activated!</p>";
	} 
	else 
	{
		echo "<p>Your account has not been activated! Wrong key/link?</p>";
	}
}
else
{
	echo "<p>No activation key present</p>";
}
?>