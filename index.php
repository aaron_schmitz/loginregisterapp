<?php 
/* Login and register platform by Aaron Schmitz using PHP 5.3.28 on IIS via FastCGI */
// DEBUG
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>Register and login platform</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="icon" type="image/icon" href="favicon.ico" />
	</head>
	<body>
		<div id="navigation">
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="?pageid=login">Login</a></li>
				<li><a href="?pageid=register">Register</a></li>
			</ul>
		</div>
		<div id="container">
			<?php 
			if(!isset($_GET['pageid']))
			{
				include('home.php');
			}
			else if($_GET['pageid'] === "login")
			{
				include('login.php');
			}
			else if($_GET['pageid'] === "register")
			{
				include('register.php');
			}
			else if($_GET['pageid'] === "validate")
			{
				include('validate.php');
			}
			else if($_GET['pageid'] === "safe")
			{
				include('safe.php');
			}
			else
			{
				include('home.php');
			}
			?>
		</div>
	</body>
</html>