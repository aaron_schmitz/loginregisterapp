<?php
	// Initalization
	// Get ready for DB connection
	require('constants.php');
	
	// Helpers
	// Email validation
	function validateEmail($email) 
	{
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	// Check if email has not already been registered
	function emailRegistered($mail)
	{
		try
		{
			$check = urlencode($mail);
			$pdo = new PDO('mysql:host='.HOST.';dbname='.DBNAME.';charset=utf8', DBUSER, DBPW);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$stmt = $pdo->prepare("SELECT * FROM user WHERE email = :email");
			$stmt->bindParam(':email', $check);
			$stmt->execute();
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}

		if($stmt->rowCount() > 0)
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}
	
	// Server-side validation of inputs starts here
	// Check for present inputs in all form fields
	if(isset($_POST['firstname']) && !empty($_POST['firstname']) AND isset($_POST['lastname']) && !empty($_POST['lastname']) AND isset($_POST['mail']) && !empty($_POST['mail']) AND isset($_POST['password']) && !empty($_POST['password']) AND isset($_POST['passwordc']) && !empty($_POST['passwordc']))
	{
		// Do password and password confirmation match?
		if($_POST['password'] !== $_POST['passwordc'])
		{
			echo "Password and password confirmation do no match!";
		}
		else
		{
			// Server-side check whether the email matches the standard email pattern
			if(!validateEmail($_POST['mail']))
			{
				echo "The email you have provided does not match the common email pattern: a@b.com!";
			}
			// If the email is already present in the database, refuse to create a new dataset
			else 
			{
				if(emailRegistered($_POST['mail']))
				{
					echo "The email you have provided has already been registered!";
				}
				// Everything has been checked, now try to insert data into DB
				else
				{
					// Generate password hash
					// NOTE: I'm using PHP 5.3.28 which does not allow me to use "password_hash()" /  BCRYPT unfortunately.
					// This is a security issue because simply hashing a password is not a safe approach
					// I could upgrade to PHP 5.5 but I'm not sure whether all other projects on my IIS will keep on working properly afterwards
					
					$pw_encrypt = urlencode($_POST['password']);
					$pw = hash("sha256", $pw_encrypt);
					
					// Encrypt the rest of the fields and
					// Generate random confirmation link to activate the account later
					$firstname_encrypt = urlencode($_POST['firstname']);
					$lastname_encrypt = urlencode($_POST['lastname']);
					$email_encrypt = urlencode($_POST['mail']);
					// Add unique string to build a unique hash
					$special = 'interoneTestProgram';
					$link = md5($email_encrypt.$special);
					
					// Try to insert into DB
					try
					{
						$pdo = new PDO('mysql:host='.HOST.';dbname='.DBNAME.';charset=utf8', DBUSER, DBPW);
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
						$stmt = $pdo->prepare('INSERT INTO '. DBTABLE .' (firstname, lastname, email, pw, link) VALUES (:firstname, :lastname, :email, :pw, :link)');
						$stmt->bindParam(':firstname', $firstname_encrypt);
						$stmt->bindParam(':lastname', $lastname_encrypt);
						$stmt->bindParam(':email', $email_encrypt);
						$stmt->bindParam(':pw', $pw);
						$stmt->bindParam(':link', $link);
						$stmt->execute();
						
					}
					catch(PDOException $ex)
					{
						echo $ex->getMessage();
					}
					
					// Database entry created, now send validation link via email
					
					try
					{
						// NOTE: not the best way to send emails
						// PHP-Mailer is a better solution but for simplicity reasons mail() is used here
						// The link is a combined string of the url of the server to a validate.php file
						// validate.php checks whether the hash of the clicked link from the sent email is present in the DB
						// If the hash exists, the account is set to active and the user is then able to log in
						$url = "http://www.aaronschmitz.de/?pageid=validate&key=";
						$url .= $link;
						$recipient = $_POST['mail'];
						$title = "Your registration link";
						$from = "From: Aaron Schmitz <info@aaronschmitz.de>\n";
						$from .= "Reply-To: info@aaronschmitz.de\n";
						$from .= "Content-Type: text/html\n";
						$text = "If you click on the link below your account should be activated!\n";
						$text .= "<br><a href=\"".$url."\">".$url."</a>";
						
						mail($recipient, $title, $text, $from);
					}
					catch(Exception $ex)
					{
						echo $ex->getMessage();
					}
					
					echo "Registration complete! Please check your mails to activate your account!";
				}
			}
		}
	}
?>

<h1>Register</h1>
<form action="" method="post" onsubmit="return validate(this);">
	<table>
		<tr>
			<td><label for="firstname">First name:</label></td>
			<td><input type="text" id="firstname" name="firstname"></td>
		</tr>
		<tr>
			<td><label for="lastname">Last name:</label></td>
			<td><input type="text" id="lastname" name="lastname"></td>
		</tr>
		<tr>
			<td><label for="mail">Email:</label></td>
			<td><input type="text" id="mail" name="mail"></td>
		</tr>
		<tr>
			<td><label for="password">Password:</label></td>
			<td><input type="password" id="password" name="password"></td>
		</tr>
		<tr>
			<td><label for="passwordc">Password confirmation:</label></td>
			<td><input type="password" id="passwordc" name="passwordc"></td>
		<tr>
			<td><input type="submit" id="button" name="button" value="send"></td>
		</tr>
	</table>
</form>

<script>
	// JavaScript validation of user inputs
	function validate(t)
	{
		var firstname = t.firstname.value;
		var lastname = t.lastname.value;
		var mail = t.mail.value;
		var password = t.password.value;
		var passwordc = t.passwordc.value;
		
		if(firstname == null || lastname == null || mail == null || password == null || passwordc == null)
		{
			alert("Bitte alle Felder ausfüllen bzw. bitte wählen Sie einen Grund aus!");
			return false;
		}
		else
		{	
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var check = re.test(mail);
			if(check)
			{
                if(password !== passwordc)
				{
					alert("Password and password confirmation do no match!");
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				alert("The email adress you have entered does not match the pattern: a@b.com!");
				return false;
			}
		}
	}
</script>